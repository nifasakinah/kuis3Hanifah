from django.shortcuts import render
from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest

from lists.views import home_page
# Create your views here.
class HomePageTest(TestCase):

	def home_page(request):
	    return HttpResponse('<html><title>masih error</title></html>')

	def test_home_page_returns_correct_html(self):
        request = HttpRequest()  #1
        response = home_page(request)  #2
        self.assertTrue(response.content.startswith(b'<html>'))  #3
        self.assertIn(b'<title>masih error</title>', response.content)  #4
        self.assertTrue(response.content.endswith(b'</html>'))  #5    